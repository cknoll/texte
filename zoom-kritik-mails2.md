## Meine 1. Mail: (2021-06-25 10:58)

Sehr geehrte Frau Geese, sehr geehreter Herr Giegold, sehr geehrte Damen
und Herren,

 

über das soziale Medium Mastodon erreichte mich der Hinweis auf die
interessant klingende Veranstaltung:

https://sven-giegold.de/europe-calling-ueberwachungskapitalismus/

 

Als lokaler Aktivist sowohl in der Nachhaltigkeits- als auch in der
digitalen Bürger:innenrechtsbewegung (unter anderem bei Bits&Bäume)
finde ich das Thema sehr spannend. Um so enttäuschter bin ich, dass
dieses Veranstaltung über Zoom stattfindet.

In meiner sozialen Blase ist die unkritische Nutzung von Zoom zum
aktuell prominentesten **Symbol für die fatale Entwicklung, welche die
Digitalisierung nimmt** geworden: Einige wenige Monopol-Konzerne
kontrollieren de facto unsere gesamte Kommunikation und dringen aktuell
auch ganz tief ins Bildungssystem und die Zivilgesellschaft ein (vom
Fernunterricht bis zum "Umwelt-Quiz").

Ein transparenter Umgang mit dem Problem, zum Beispiel durch eine
einordnende distanzierende Fußnote, würde die Sache schon besser machen.
Es geht ja, neben der konkreten Gefährdung für die informationelle
Selbstbestimmung, auch um die Vermeidung der *symbolische Botschaft*:
Die Organisation XYZ benutzt Zoom – dann scheint (ihr) das Thema
Datenschutz also nicht so wichtig zu sein.

Defacto-Monopolstrukturen wie Zoom zeichnen sich durch kritische
Machtkonzentration aus. Und wo Macht ist – das lehren die Geschichte und
Gegenwart – besteht immer auch die Gefahr von Machtmissbrauch.

Das unkritische Nutzen (und damit unvermeidliche bewerben!) solcher
Dienste ist also **Teil des Problems**. Von den Grünen im Europäischen
Parlament erwarte ich aber – und ich denke das sind wir uns einig – dass
sie ihr möglichstes tun, um **Teil der Lösung** zu sein.

 

Diese Mail soll dazu beitragen, die Stimmung an der
zivilgesellschaftlichen Basis in die politische Sphäre zu kommunizieren.

 

Beste Grüße aus Dresden,
...


---

## Antwort des zuständigen Mitarbeiters von Sven Giegold (2021-06-25 11:13)


vielen Dank für Ihr Schreiben. Wir setzen uns in der Tat sehr kritisch mit Zoom und anderen Webinaranbietern auseinander. Deswegen haben wir vertraglich mich Zoom geregelt, dass keine der Daten zu unseren Webinaren die Europäische Union verlassen und damit jederzeit der DSGVO unterliegen. Das sehen Sie an dem „eu“ im Link. Zudem ist geregelt, dass Zoom die Daten nur zur Bereitstellung des Webinars verwendet und nicht weiterverwenden darf. In dieser Konfiguration empfehlen die Datenschutzexpert:innen unserer Fraktion die Nutzung von Zoom als datenschutz-freundlich.

 

Wir sind trotzdem immer auf der Suche nach anderen Webinar-Plattformen, die stabil, nutzer:innen- und datenschutzfreundlich mehrsprachige Webinare mit bis zu 3000 Teilnehmer:innen durchführen können. Leider gibt es unseren Wissens hierzu gerade keine Alternative. Wenn Sie eine kennen, würden wir uns sehr über einen Hinweis freuen.

 

Letztlich setzen wir uns auch für starke gesetzliche Regeln für Digitalunternehmen wie Zoom ein, damit die gesamte Plattformlandschaft sich wie skizziert verändert.

 

Vielen Dank und schöne Grüße,
...


---

## Meine 2. Mail: (2021-06-25 10:58)


Sehr geehrter Herr ...,


Ihre Antwort kann mich bisher nicht überzeugen.

Dem Link

"https://eu01web.zoom.us/...."

entnehme ich als relevantester Teil die Domain: zoom.us. Das ist ein
US-amerikanisches Börsennotiertes Unternehmen. Welches

1. In der Vergangenheit durch Sicherheitslücken und mangelenden
Datenschutz aufgefallen ist,

2. Eine ungesunde Quasi-Monopolstellung inne hat,

3. Im datenschutzsensiblen Teil der Gesellschaft zu einem echten
Reizwort avanciert ist.


Interessehalber habe ich mittels des Dienstes

https://www.site24x7.com/de/tools/finden-ihres-domanenorts.html

Den Serverstandort der Subdomain eu01web.zoom.us angefragt. Ergebnis:
San Jose (Kalifornien, USA). Siehe Anhang.


Ob das ein temporäres Ergebnis oder ein methodisches Problem meiner
Anfrage ist, kann ich gerade nicht beurteilen, aber es würde mich nicht
überraschen, wenn Zoom datenschutzbezogene Zusagen schlicht ignoriert.


Hinzu kommt, selbst mit Europäischem-Server-Standort:

Meiner Kenntnis nach haben US-Geheimdienste legal Zugriff auf alle Daten
von US-IT-Firmen (unabhängig vom Server-Standort)


In den USA herrscht ein politisches System in dem schon die Einführung
eine Krankenversicherung als kommunistische Dystopie geframed wird (!).
Spätestens seit Snowden ist bekannt, dass die US-Geheimdienste auch die
wirtschaftlichen Interessen von US-Unternehmen versuchen durchzusetzen
und weiterhin ist bekannt, dass IT-Unternehmen Feindeslisten erstellen
(z.B. Facebook).

Vordiesem Hintergrund ist es zumindet hochgradig unsensibel,
möglicherweise aber auch fahrlässig, eine  Veranstaltung, welche schon
im Titel zwei 100%-Reizworte ("Überwachung" und "Kapitalismus") trägt,
über eine zoom.us-URL abzuwickeln. Leichter ist man selten an die Daten
potenzieller Gegner:innen rangekommen.



Zitat aus Mastodon:

"""
Die diskreditieren sich in ihrem Fachgebiet selbst. Normalerweise nichts
was jemand im Wahlkampf machen würde.
Mit so einem Thema erreichst du vor allem die Menschen die sich damit
beschäftigen und jetzt, aufgrund der Zoom-Nutzung die Hand vorm Kopf haben.
"""


Wenn die CDU (bzw. EVP) so eine Realsatiere-Aktion bringen würde ...
Geschenkt.. Aber aus Ihrer Ecke muss die Gesellschaft mehr erwarten können.


Zurück zur Sachebene:

Ein "Webinar" mit mehr als 200 TN ist konzeptionell ohnehin fragwürdig.
Das ließe sich besser (und ressourcensparender) als Stream lösen. Den
Rückkanal bildet dann z.B. ein Chat.

Eine weitere Alternative ist goto-Meeting:

https://www.goto.com/de/webinar

Das ist zwar auch eine US-Firma aber immerhin nicht der Quasimonpolist.


Beste Grüße,
...

Anhang: ![Screenshot: Server Standort USA](img/Screenshot_20210625_160032.png "Screenshot: Server Standort USA")


---

## 2. Antwort (2021-06-27 21:09)


Sehr geehrter ...



unsere Datenschutzexpert:innen im Europaparlament – dieselben, die die DSGVO maßgeblich mitgeschrieben und verhandelt haben – haben die großen Webinar-Anbieter ausführlich miteinander verglichen. Insbesondere auch im Vergleich mit GotoWebinar, der Nummer 2, auch aus den USA. Ihr Fazit: Im April 2020 hat Zoom auf die Kritik reagiert und die Datenschutzbestimmungen stark verbessert, so dass es DSGVO-konform wurde. Deswegen sind wir damals umgestiegen, weil GotoWebinar das nicht gemacht hat und nicht DSGVO-konform genutzt werden kann. Wenn Sie also jetzt empfehlen auf GotoWebinar umzusteigen, dann empfehlen Sie damit auch einen schlechteren Datenschutz. Es ist leider nicht alles schwarz und weiß und alle Kritik, die gerade im digitalen Raum millionenfach potenziert, aber selten wieder korrigiert wird, trifft unverändert und immer gleich zu. So ist das auch bei Zoom. Plattformen verändern sich, und das genau ist ja auch das Ziel von politischer und zivilgesellschaftlicher Arbeit. Leider ist das nicht immer sehr transparent. Sie sind nicht der erste, der uns dazu schreibt, und auch nicht der erste der GotoWebinar als vermeidliche Alternative empfiehlt.



Sie können sich sicher sein, wir nehmen die Bedenken sehr ernst und schauen regelmäßig, welche Alternativen es gibt und wählen die datenschutzfreundliche.

Ihren Kommentar zur konzeptionellen Sinnhaftigkeit von Veranstaltungen mit über 200 TN kann ich leider überhaupt nicht teilen. Waren Sie denn schonmal bei einem mehrsprachigen Webinar von uns dabei, mit 1000 Leuten in einem Raum? Wir bekommen durchgängig das Feedback , dass gerade die Möglichkeit sich per Wortmeldung direkt einzuschalten (nicht nur über den Chat) und viel direkter dabei zu sein als in einem Stream (der zudem nur mit hohen Zusatzkosten mehrsprachig werden könnte) das Spannende und Verbindende des Formates ausmacht. Daran werden wir also sicher festhalten.

Vielen Dank und freundliche Grüße,

...


---

## Meine 3. Mail (2021-06-28 20:12)


Sehr geehrter ...

vielen Dank für Ihre ausführliche Rückmeldung.

Dieser entnehme ich, dass Sie sich ausführlich mit dem "Zoom-Problem"
befasst haben, was ich grundsätzlich sehr begrüße.


Gerade habe ich auch nochmal den Server-Standort der Subdomain
eu01web.zoom.us abgefragt und bekomme *jetzt*: Frankfurt am Main.

Den DSGVO-Zusicherungen von Zoom zum Trotze halte ich es für
"suboptimale" Öffentlichkeitsarbeit diese Veranstaltung mit diesem Titel
exklusiv auf dieser Plattform abzuwickeln.


Um auf Ihre Frage zu antworten: Ich war noch nie in einem
1000-Personen-"Webinar". Ich kann mir aktuell schwer vorstellen, wie das
konstruktiv gestaltet werden kann, wie als entschieden wird, wer wie
lange reden darf. Aber offenbar haben Sie bei Ihren bisherigen
Veranstaltungen damit gute Erfahrungen gemacht.

Die Diskussion, und insbersondere Ihre (subjektiv nachvollziehbare)
Ablehnung einer Suffizienzstrategie: "... das Spannende und Verbindende
des Formates ausmacht. Daran werden wir also sicher festhalten" zeigt
übrigens ein grundsätzliches Dilemma:


Die Erwartungshaltung über die Leistungsfähigkeit von Produkten wird
durch Akteure definiert, die – fokussiert auf Profitmaximierung –
nachhaltigkeitsbezogene Aspekte allenfalls nachrangig oder unter Druck
berücksichtigen, wenn überhaupt. Das gilt z.B. auch im Bereich
Mobilität: Natürlich ist es bequemer (besser) per Buissiness-Class von
Berlin nach Paris zu fliegen, als mit einem gut ausgelasteten Nachzug zu
reisen, aber das kann gesamtgesellschaftlich nicht die einzige relevante
Kategorie sein.

Meine Kritik an Zoom als Plattform für politische Arbeit (und Bildung)
speißt sich aus der normativen Erwartungshaltung, dass Menschen bzw.
Organisationen, die "Überwachungskapitalismus" immerhin schon mal als
relvantes Problem erkannt haben, dann konsequenterweise auch in Ihrem
konkreten Handeln einen "Sicherheitsabstand" zu den tatsächlich oder
zumindest symbolisch problematischen Akteuren dieses Systems einhalten
und sich nach Kräften um Dezentralität und auf Freier Software basierten
Lösungen bemühen. Analog wie Greta Thunberg, die eben – auch unter in
Kaufnahme hohen persönlichen Aufwandes – auf eine Anreise zum
Klimagipfel per Flugzeug verzichtet hat.


Beste Grüße,
...

---

## Antwort auf meine 3. Mail

vielen Dank! Wir gehen an die Frage der Nutzung von Technologie etwas anders heran. Für uns macht es politisch keinen Sinn unsere Reichweite durch die Nutzung weniger geeigneter Technologie zu beschränken. Also z.B. kein Zoom, dafür das deutsche Edudip, dass bei 200-300 Teilnehmer*innen in die Knie geht. Oder z.B. auf Facebook oder Twitter zu verzichten. Wir nutzen diese Plattformen um möglichst viele Leute zu erreichen und von unseren politischen Plänen zu überzeugen, zu der ja eine grundsätzliche Änderung dieser genannten Plattformen gehören. Wenn Sie am 28.6. dabei waren, da wurde das auch sehr klar. Können Sie auch hier nachschauen: https://sven-giegold.de/europe-calling-ueberwachungskapitalismus/

 

Die Alternative wäre diese Plattformen nicht zu nutzen, damit weniger Menschen zu erreichen und dann am Ende weniger Unterstützung für unsere Pläne zu bekommen. Dann würde einfach alles so bleiben wie es ist, weil Union/SPD/FDP einfach so weitermachen. Damit wäre unser Boykott vollkommen sinnlos. Bei der Europawahl haben wir gesehen, was für ein Unterschied 20% Grüne machen. Den Green Deal und jetzt die Reform der Digitalmärkte hätte es so nicht gegebenen. Deswegen werden wir weiter die besten Wege nutzen (natürlich weiterhin DSGVO-konform wie wir z.B. Zoom nutzen), um Veränderung zu erreichen.

 

Ihnen nochmals vielen Dank und alles Gute,
...

---
