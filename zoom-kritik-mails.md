(Namen von Individuen und Organisiationen durch "ABC", "OPQ", "XYZ" etc. ersetzt)

---

Lieber OPQ Dresden, liebe Liste,


vielleicht haben einige schon drauf gewartet, aber das hier

On 20.01.21 16:34, XYZ | OPQ Dresden wrote:

> wir wollen euch daran erinnern, dass nächste Woche Donnerstag, *dem 28.
> Januar,*  *19 Uhr* der FARN*-Referent Felix Schulz einen
> *OPQwissen-Vortrag zum Thema "Rechtsextremismus und Naturschutz"* hält.
> Zugangslink:       https://zoom.us/j/96928718560
kann aus meiner Sicht nicht ohne öffentlichen[^1] Widerspruch stehen
bleiben.

„Wer aber vom Kapitalismus nicht reden will, sollte auch vom Faschismus
schweigen“ (Max Horkheimer)

... und ⚡Zoom⚡ ist eines der prominentesten Symbole des modernen
Überwachungskapitalismus.


Weil die beiden längeren Antwortmails vom WE auf eine
Veranstaltungsankündigung der OPQjugend (siehe unten) vielleicht
untergegangen sind, habe ich das Problem und mögliche Lösungen hier
nochmal in einem kurzen Foliensatz zusammengefasst:


https://hackmd.okfn.de/p/Syb_Y68yd#/


Fazit: Vom OPQ muss und kann die Welt im Jahr 2021 mehr erwarten, als
Veranstaltungen über Zoom zu bewerben.

Bitte nicht als demotivierende Grundsatzkritik auffassen, sondern als
ehrlich wohl gemeinten Hinweis auf ein relevantes Problem.



[^1] Ich habe mich (wieder) für eine Mail über die Liste entschieden,
weil die Multiplikationswirkung der Veranstatlungsankündigung (defacto
Zoom-Werbung) Kern des Problems ist und demnach der Widerspruch auch
entsprechender Multiplikationswirkung bedarf.


Beste Grüße,
ABC

Es folgen chronologisch die beiden themenbezogenen Mails von letztem
Samstag und Sonntag (gemäß der Rückmeldungen nicht ganz ohne
Unterhaltungswert):




On 16.01.21 15:46, ABC wrote:


> Lieber EFG, liebe OPQjugend, liebe Oeko-DD-Empfänger:innen,
> 
> als Mitbegründer dieses Mailverteilers und als Mitbegründer von Bits-
> und Bäume Dresden sehe ich mich in der moralischen Verpflichtung aus der
> schweigenden Masse herauszutreten und zu das Wort zu ergreifen:
> 
> 
> Umwelt Quiz? Super Sache!
> 
> ...
> 
> Aber über *Zoom*? Was zum Kranich habt Ihr denn gefrühstückt?
> 
> 
> Aber mal im Ernst. Zoom ist echt nicht cool [1, 2]. Und eine coole Orga
> wie die OPQjungend sollte sowas nicht nutzen, erst recht nicht für eine
> öffentliche Veranstaltung für die breit geworben wird. 1. Weil es
> massive objektive Probleme Datenschutzprobleme gibt, die sich die
> OPQjugend nicht einhandeln sollte. 2. Wegen der Vorbildwirkung (und der
> daraus resultierenden Verantwortung) und 3. Weil die Organisation sich
> damit selber unglaubwürdig macht.
> 
> Eine unkritische ZOOM-Nutzung ist inkonsistent mit dem inhaltlichen
> Anliegen der Veranstaltung und der Bewegung für einen
> emanzipatorisch-progressiven Wandel insgesamt. Es wirkt ein bisschen,
> als ob eine FossilFree-Gruppe zum Plenum in ein Tankstellen-Bistro einlädt.
> 
> Ich vermute, die bisherige Unsensibilität diesbezüglich liegt an einer
> Mischung aus wahrgenommenen Sachzwängen und fehlenden Infos über die
> Probleme und die Alternativen – also in etwa die gleiche Gemengelage,
> wie bei den meisten anderen großen und kleineren gesellschaftlichen
> Missständen auch.
> 
> Aber: Datenschutz ist im gesellschaftlichen Aktivismus des 21.
> Jahrhunderts kein nerviger Ballast, sondern muss Kernkompetenz sein!
> Erst recht in einem Phase, in der die freiheitliche Demokratie weltweit
> und auch bei uns erodiert wie ein Schneemann im Regen.
> 
> 
> Das absolut mindeste wäre, in der Veranstaltungsankündigung eine Fußnote
> einzufügen:
> 
> """
> Wir sind uns der mit der Videokonferenzplattform ZOOM verbundenen
> Probleme bewusst und prüfen derzeit Alternativen für zukünftige
> Veranstaltungen.
> """
> 
> Das macht erst mal keine zusätzliche Arbeit, zeigt aber immerhin
> Problembewusstsein und Lernbereitschaft – beides wichtige Eigenschaften
> in einer Transformation zu einer besseren Welt.
> 
> Weitere Infos:
> 
> [1] https://dresden.bits-und-baeume.org/2020/05/22/zoom-problematik.html
> 
> [2]
> https://discourse.bits-und-baeume.org/t/zivilgesellschaftliche-organisationen-und-zoom-nutzung/595
> 
> Empfohlene Alternative zu Zoom: BigBlueButton,
> 
> z.B. via
> 
> https://bbb.agdsn.de/ oder https://senfcall.de/
> 
> 
> 
> Zur Einordnung dieser Mail: Ein Anliegen der Bits&Bäume-Bewegung ist es,
> die Akteur:innen der Zivilgesellschaft und insbesondere der
> Umweltbewegung bei einer nachhaltigen Nutzung und Mitgestaltung
> digitaler Strukturen zu unterstützen und für die entsprechenden Probleme
> zu sensibilisieren.
> 
> https://dresden.bits-und-baeume.org/selbstverstaendnis.html
> 
> 
> Für (konkrete oder allgemeine) Rückfragen stehen wir gerne zur Verfügung.
> 
> Emanzipatorisch-Progressive Grüße und bleibt gesund,
> 
> ABC
> 
> 
> 
> PS: Ich gehe jetzt einen Schneeman bauen!
- - - - -


On 17.01.21 15:13, ABC wrote:


> Lieber EFG, liebe OPQjugend, liebe Oeko-DD-Empfänger:innen,
> 
> Nach einer Nacht drüber schlafen und verschiedenen Arten von Feedback,
> halte ich es für angemessen, meine gestrige Nachricht nochmal einzuordnen:
> 
> Es lag und liegt überhaupt nicht in meiner Absicht die OPQjugend oder
> das Projekt Umweltquiz zu diskreditieren. Ich weiß, wie viel Arbeit und
> Energie in ehrenamtliches Nachhaltigkeitsengagement fließt. Im
> Gegenteil: Ich finde Euch und das Projekt super! Nur deshalb habe ich
> mir die Mühe gemacht und versucht, eine – der teilweise jugendlichen
> Zielgruppe geschuldeten – humorvolle Mail zu verfassen, um auf ein aus
> meiner Sicht reales Problem hinzuweisen.
> 
> Ob das Stilmittel der augenzwinkernden ⚡ Zuspitzung ⚡ (!! WTF !!),
> jedoch bei allen als solches angekommen ist, daran bestehen inzwischen
> begründete Zweifel.
> 
> Also nochmal (fast) humorlos:
> 
> - Ich halte Datentschutz für ein sehr wichtiges Konfliktfeld in der
> politischen Auseindandersetzung über die Welt von morgen. Macht setzt
> Wissen voraus und diese Wissen kommt heutzutage zum großen Teil aus
> Daten, die "freiwillig" zur Verfügung gestellt werden. Die StaSi hätte
> sich die Finger wund geleckt, wenn sie auch nur ein Zehntel der Daten
> der heutigen IT-Monopolisten gehabt hätte.
> 
> - Ich halte es für wünschenswert, dass sich Individuen und
> Organisationen, die sich hauptsächlich in bestimmten Bereich (z.B.
> Umwelt) für eine bessere Welt einsetzen, auch eine Sensibilität in
> anderen Bereichen (z.B. Antidiskreminierung, digitale Selbstbestimmung,
> ...) entwickeln.
> 
> - Dass dabei Zielkonflikte, unterschiedliche Meinungen, "Fehler" und
> "Kritik" auftreten, ist abe rvöllig selbstverständlich. Wenn die Lösung
> der globalen Probleme einfach wäre, dann hätten wir sie heute nicht. Das
> ganze kann nur in einem permanenten Lernprozess (für alle)
> funktionieren. Und mit Lernen meine ich: Informationen austauschen.
> Meine Mail sollte ein Beitrag dazu sein.
> 
> - Zoom ist auch heute, einen Tag später, immernoch problematisch [1].
> Vorallem aber steht für manche Menschen, die sich mit der Materie
> beschäftigt haben, die unkritische Nutzung von Zoom als aktuell
> prominentestes *Symbol für die fatale Entwicklung, welche die
> Digitalisierung nimmt*: Einige wenige Monoplo-Konzerne kontrollieren de
> facto unsere gesamte Kommunikation und dringen aktuell auch ganz tief
> ins Bildungssystem ein (vom Fernunterricht bis zum "Umwelt-Quiz"). Und
> weite Teile der Bevölkerung und leider auch der aktiven
> Zivilgesellschaft zucken mit den Schultern und sind vom süßen Gift der
> fluffigen Klicki-Bunti-User-Experience so verwöhnt, dass sie weniger
> problematische Alternativen als unzumutbar bucklig ablehnen. Und in
> gewisser Weise habe sie damit sogar recht, weil die Standards der
> Zumutbarkeit eben durch die gestylten Monopollösungen definiert werden
> (so wie die Standards bezüglich Reisezeit (und -preis) von Berlin nach
> Barcelona eben vom Flugzeug und nicht von der Bahn oder gar dem Fahrrad
> bestimmt sind.)
> 
> - Die Einsicht in diese Entwicklung verursacht Frustration und oft auch
> Resignation, und die wird dann jedes mal getriggert, wenn das Wort
> "Zoom" in unkritischem Zusammenhang benutz wird.
> 
> - Ein transparenter Umgang mit dem Problem, zum Beispiel mit der in der
> ersten Mail vorgeschlagenen Fußnote, macht die Sache schon deutlich
> besser. Es geht ja, neben der konkreten (vorhandenen aber
> überschaubaren) Gefährdung für die informationelle Selbstbestimmung, wie
> gesagt hauptsächlich um die Vermeidung der *symbolische Botschaft*: Die
> OPQjungend benutzt Zoom – dann scheint (ihr) das Thema Datenschutz also
> nicht so wichtig zu sein.
> 
> - Die ganze Sache hier bestärkt mich darin, dass das Anliegen von
> Bits-und-Bäume, nämlich der kommunikative Brückenschlag zwischen
> Umweltbewegung und digitaler Zivilgesellschaft notwendiger denn je ist,
> deswegen der spontane Vorschlag:
> 
> Wollen wir mal zusammen ein Quiz organisieren? Mit Fragen aus beiden
> Bereichen? Und mit PR in beiden Blasen? Und dann möglichst nicht über
> Zoom? Ich wäre dafür!
> 
> - Gerne können wir auch mal eine sachliche inhaltliche Diskussion
> darüber führen, ob Zoom, WhatsApp, Insta etc. wirklich so schlimm sind,
> oder ob die Nerds da nicht völlig überzognen #DatenAlarmismus betreiben.
> 
> - Sollte es noch Fragen oder Gesprächsbedarf geben: einige Leute aus dem
> OPQ haben meine Telefonnummer, ansonsten auch gerne Nummerntausch per
> Mail. Schriftliche Kommunikation hat eben Grenzen.
> 
> 
> In jedem Fall, vielen Dank für Eure Arbeit und Eurer Engagement. Niemand
> ist perfekt, und das gibt uns allen Raum, voneinander zu lernen – und
> dabei den Spaß nich aus den Augen zu verlieren.
> 
> 
> 
> Liebe Grüße,
> ABC
> 
> 
> PS: Mir ist gestern beim Rollen der ersten Schneekugel aufgefallen, dass
> ich gar keinen "Schneemann" sondern eigentlich einen "Schneemensch"
> bauen wollen sollte. Ich mach da jetzt mal weiter...