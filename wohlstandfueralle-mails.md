Sehr geehrter Herr Nymoen, sehr geehrter Herr Schmitt

in den Episoden 103 und 105 ihres Podcasts [1] haben Sie sich mit dem
Thema Postwachstum befasst. Dabei haben Sie viele wichtige Punkte
angesprochen. Allerdings erscheint mir Ihre Kritik doch reichlich
verkürzt [2].

Wenn ich Paech richtig verstanden habe, ist seine zentrale These:
Dauerhaftes exponentielles Wachstum innerhalb der planetaren Schranken
ist nicht möglich. Folglich wird eine Postwachstumsgesellschaft so oder
so kommen ("by design or by disaster").


Deshalb macht er sich Gedanken, wie eine solche Gesellschaft aussehen
könnte. Die Vorschläge zu denen er damit kommt, wirken gemessen an
unseren heutigen "kulturell induzierten Ansprüchen an materielle
Selbstverwirklichung" hart. Das bestreitet Paech auch nicht ("Eine
Postwachstumsgesellschaft ist keine Bequemokratie").

Das Drohpotenzial dieses "Design-Szenarios" relativiert sich allerdings,
wenn man es mit dem "Disaster-Szenario" vergleicht: dem
Kapitalismus-induzierten Zusammenbruch der Zivilisation.



Man muss Paech und der PW-Bewegung zu Gute halten, dass Sie zumindest
versuchen, konstruktiv Zukunftsszenarien zu entwerfen, die physikalisch
kompatibel mit den planetaren Grenzen sind (von denen das Klima
bekanntermaßen nur eine darstellt). Ihre Kritik dagegen scheint in
weiten Teilen schlicht weg von (eignen und postulierten)
Akzeptanzproblemen getrieben.


Das könnte ich akzeptieren, wenn Sie einen überzeugenden Gegenentwurf
liefern würden. Stattdessen bleibt Ihre Kritik rein destruktiv ("das
kann niemand wollen"). Allenfalls ziehen Sie sich auf das Hoffen auf
Technologie und Entkopplung zurück.

Kann man machen, aber dann ist man auf dem Überzeugungskraft-Niveau von
"Digitalisierung first, Bedenken Second" angekommen.


Aus meiner Sicht, sind Sie mit diesen beiden Episoden dem
Postwachstumsansatz inhaltlich nicht auf dem von Ihnen angestrebten
Niveau gerecht geworden. Ich möchte deshalb anregen, dass Sie einer
Vertreter:in dieser Perspektive explizit eine Stimme geben, zum Beispiel
in einem Interview oder konstruktiven Streitgespräch. Ich hoffe, dass
dann einige Verzerrungen und Verkürzungen geradegerückt werden könnten
und die validen Punkte beider Positionen um so klarer hervortreten.



Aus meiner Sicht ist es eine bedauernswerte Tradition, dass zwischen den
verschieden Lagern des "menschenfreundlichen Teils der Gesellschaft"
eine recht aggressive Rhetorik herrscht. Leider scheinen auch Sie sich
bisher nicht von dieser Tradition zu emanzipieren. Angesichts der
gewaltigen gesellschaftlichen Herausforderungen halte ich es für sowohl
für moralisch als auch für utilitaristisch geboten, persönliche
Befindlichkeiten gegenüber ernsthaftem Bemühen um objektive
Problemlösung herabzupriorisieren.


Beste Grüße,
...


[1] https://wohlstandfueralle.podigee.io/105-niko-paech


[2] Hierzu nur ein Beispiel: Am Schluss von 105 (bei 00:41:10h) nehmen
Sie den bisherigen Rückgang von 1.2 Gt auf 812 Mt als Beleg dafür dass
eine Entkopplung zwischen CO2-Ausstoß möglich ist, verschweigen aber (im
Gegensatz zu Paech), dass in diesem Zeitraum ein großer Teil der
schmutzigen Industrieproduktion ins Ausland abgewandert ist und deshalb
aus unserer Bilanz rausfällt. Das spricht aber nicht für die Sauberkeit
unserer gesamtwirtschaftlichen Wertschöpfungskette.
